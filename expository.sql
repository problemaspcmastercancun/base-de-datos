-- MySQL Script generated by MySQL Workbench
-- Thu Mar  5 11:57:29 2020
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema expository
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema expository
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `expository` DEFAULT CHARACTER SET utf8mb4 ;
USE `expository` ;

-- -----------------------------------------------------
-- Table `expository`.`evaluadores`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `expository`.`evaluadores` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `Nombres` VARCHAR(45) NULL,
  `Apellidos` VARCHAR(45) NULL,
  PRIMARY KEY (`Id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `expository`.`exposiciones`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `expository`.`exposiciones` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Titulo` LONGTEXT NULL DEFAULT NULL,
  `Descripcion` LONGTEXT NULL DEFAULT NULL,
  `EvaluadorId` INT NOT NULL,
  PRIMARY KEY (`Id`),
 CONSTRAINT `fk_exposiciones_evaluadores1`
    FOREIGN KEY (`EvaluadorId`)
    REFERENCES `expository`.`evaluadores` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `expository`.`grupos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `expository`.`grupos` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Nombre` LONGTEXT NULL DEFAULT NULL,
  `Descripcion` LONGTEXT NULL DEFAULT NULL,
  PRIMARY KEY (`Id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `expository`.`ponentes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `expository`.`ponentes` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Nombres` LONGTEXT NULL DEFAULT NULL,
  `Apellidos` LONGTEXT NULL DEFAULT NULL,
  `Matricula` LONGTEXT NULL DEFAULT NULL,
  `GrupoId` INT(11) NOT NULL,
  PRIMARY KEY (`Id`),
CONSTRAINT `FK_Ponentes_Grupos_GrupoId`
    FOREIGN KEY (`GrupoId`)
    REFERENCES `expository`.`grupos` (`Id`)
    ON DELETE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `expository`.`evaluaciones`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `expository`.`evaluaciones` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `ExposicionId` INT(11) NOT NULL,
  `EvaluadorId` VARCHAR(255) NULL DEFAULT NULL,
  `PonenteId` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
   CONSTRAINT `FK_Evaluaciones_Exposiciones_ExposicionId`
    FOREIGN KEY (`ExposicionId`)
    REFERENCES `expository`.`exposiciones` (`Id`)
    ON DELETE CASCADE,
  CONSTRAINT `FK_Evaluaciones_Ponentes_PonenteId`
    FOREIGN KEY (`PonenteId`)
    REFERENCES `expository`.`ponentes` (`Id`)
    ON DELETE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `expository`.`reactivos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `expository`.`reactivos` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Titulo` LONGTEXT NULL DEFAULT NULL,
  `Descripcion` LONGTEXT NULL DEFAULT NULL,
  PRIMARY KEY (`Id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `expository`.`calificaciones`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `expository`.`calificaciones` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Valor` INT(11) NOT NULL,
  `Comentarios` LONGTEXT NULL DEFAULT NULL,
  `ReactivoId` INT(11) NOT NULL,
  `EvaluacionId` INT(11) NULL DEFAULT NULL,
  `EvaluadorId` INT NOT NULL,
  PRIMARY KEY (`Id`),
  CONSTRAINT `FK_Calificaciones_Evaluaciones_EvaluacionId`
    FOREIGN KEY (`EvaluacionId`)
    REFERENCES `expository`.`evaluaciones` (`Id`),
  CONSTRAINT `FK_Calificaciones_reactivos_ReactivoId`
    FOREIGN KEY (`ReactivoId`)
    REFERENCES `expository`.`reactivos` (`Id`)
    ON DELETE CASCADE,
  CONSTRAINT `fk_calificaciones_evaluadores1`
    FOREIGN KEY (`EvaluadorId`)
    REFERENCES `expository`.`evaluadores` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
